# UnityUtils
Utility classes for Unity.

## RectTransformSnap
RectTransformSnap layout component snaps RectTransform to another RectTransform (snap target) according to snapping rules. Snapping rules are defined for both vertical an horizontal axes. These are RectTransformSnap.VerticalSnap and RectTransformSnap.HorizontalSnap enums.

## VerticalLayoutAlign
VerticalLayoutAlign to layout child objects vertically.