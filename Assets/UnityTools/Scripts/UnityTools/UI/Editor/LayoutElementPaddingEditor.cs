/*
The MIT License (MIT)

Copyright (c) 2016 Pavel Stupka

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using UnityEditor.UI;
using UnityEditor;

namespace UnityTools.UI {
    
    [CustomEditor(typeof(LayoutElementPadding), true)]
    [CanEditMultipleObjects]
    public class LayoutElementPaddingEditor : SelfControllerEditor {
        
        SerializedProperty m_paddingTop;
        SerializedProperty m_paddingBottom;
        SerializedProperty m_paddingLeft;
        SerializedProperty m_paddingRight;
        
        //--------------------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------------------
        //  PUBLIC
        //--------------------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------------------

        public override void OnInspectorGUI() {
            serializedObject.Update();
            EditorGUILayout.PropertyField(m_paddingTop);
            EditorGUILayout.PropertyField(m_paddingBottom);
            EditorGUILayout.PropertyField(m_paddingLeft);;
            EditorGUILayout.PropertyField(m_paddingRight);;
            serializedObject.ApplyModifiedProperties();

            base.OnInspectorGUI();
        }

        //--------------------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------------------
        //  PROTECTED        
        //--------------------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------------------
        
        protected virtual void OnEnable() {
            m_paddingTop = serializedObject.FindProperty("m_paddingTop");
            m_paddingBottom = serializedObject.FindProperty("m_paddingBottom");
            m_paddingLeft = serializedObject.FindProperty("m_paddingLeft");
            m_paddingRight = serializedObject.FindProperty("m_paddingRight");
        }
        
        //--------------------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------------------
    }
}