/*
The MIT License (MIT)

Copyright (c) 2016 Pavel Stupka

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using UnityEngine.EventSystems;
using UnityEngine;
using UnityEngine.UI;

namespace UnityTools.UI {

    [AddComponentMenu("Layout/Rect Transform Snap", 200)]
    [ExecuteInEditMode]
    [RequireComponent(typeof(RectTransform))]
    public class RectTransformSnap : UIBehaviour, ILayoutSelfController {
        
        // Snap Modes.
        public enum VerticalSnap { None, Center, TopIn, TopOut, BottomIn, BottomOut }
        public enum HorizontalSnap { None, Center, LeftIn, LeftOut, RightIn, RightOut }        

        // Snap Target.
        [SerializeField] private RectTransform m_target = null;
        public RectTransform target { get { return m_target; } set { m_target = value; SetDirty(); }}
        
        // Vertical snap.
        [SerializeField] private VerticalSnap m_verticalSnap = VerticalSnap.None;
        public VerticalSnap verticalSnap { get { return m_verticalSnap; } set { m_verticalSnap = value; SetDirty(); }}
        
        // Horizontal snap.
        [SerializeField] private HorizontalSnap m_horizontalSnap = HorizontalSnap.None;
        public HorizontalSnap horizontalSnap { get { return m_horizontalSnap; } set { m_horizontalSnap = value; SetDirty(); }}
        
        // Delta shift.
        [SerializeField] private Vector2 m_deltaShift = Vector2.zero;
        public Vector2 deltaShift { get { return m_deltaShift; } set { m_deltaShift = value; SetDirty(); }}        

        // Rect transform.
        [System.NonSerialized]
        private RectTransform m_Rect;  
        
        // Driven Tracker.
        private DrivenRectTransformTracker m_Tracker;
        
        //--------------------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------------------
        //  PUBLIC
        //--------------------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------------------                     
        
        public virtual void SetLayoutHorizontal() {}
        
        //--------------------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------------------
        
        public virtual void SetLayoutVertical() {}
        
        //--------------------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------------------
        
        public void Snap() {
            SetDirty();
        }
        
        //--------------------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------------------
        //  PROTECTED
        //--------------------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------------------
        
        protected RectTransformSnap() { }
        
        //--------------------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------------------
        
        protected override void OnEnable() {
            base.OnEnable();
            SetDirty();
        }
        
        //--------------------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------------------
        
        protected override void OnDisable() {
            m_Tracker.Clear();
            LayoutRebuilder.MarkLayoutForRebuild(rectTransform);
            base.OnDisable();
        }
        
        //--------------------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------------------
        
        protected override void OnRectTransformDimensionsChange() {
            UpdateRect();
        }
        
        //--------------------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------------------
                
        protected void SetDirty() {
            if (!IsActive()) {
                return;
            }                
            UpdateRect();
        }
        
        //--------------------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------------------
        
        #if UNITY_EDITOR
        
        protected override void OnValidate() {
            SetDirty();
        }

        #endif
        
        //--------------------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------------------
        //  PRIVATE
        //--------------------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------------------
        
        private RectTransform rectTransform  {
            get {
                if (m_Rect == null) {
                    m_Rect = GetComponent<RectTransform>();
                }                    
                return m_Rect;
            }
        }
        
        //--------------------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------------------
        
        private void Update()  {
            if (m_target != null && m_target.hasChanged) {
                SetDirty();
            }                
        }
        
        //--------------------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------------------
        
        private void UpdateRect()  {
            m_Tracker.Clear();
            
            if (m_target != null)  {
                if (m_verticalSnap != VerticalSnap.None) {
                    UpdateVerticalSnap();
                }                     
                    
                if (m_horizontalSnap != HorizontalSnap.None) { 
                    UpdateHorizontalSnap();
                }
            }
        }
        
        //--------------------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------------------
        
        private void UpdateVerticalSnap()  {                        
            // Current anchor info.
            Vector2 anchorMin = rectTransform.anchorMin;
            Vector2 anchorMax = rectTransform.anchorMax;
            Vector2 anchoredPosition = rectTransform.anchoredPosition;
            Vector2 pivot = rectTransform.pivot;
            
            // Center Snap.
            if (m_verticalSnap == VerticalSnap.Center) {
                anchorMin.y = (m_target.anchorMin.y + m_target.anchorMax.y) / 2.0f;
                anchorMax.y = anchorMin.y;
                anchoredPosition.y = m_target.anchoredPosition.y + (0.5f - m_target.pivot.y) * m_target.sizeDelta.y;
                anchoredPosition.y += m_deltaShift.y;
                pivot.y = 0.5f;
            }
            
            // Bottom Snap.
            else if (m_verticalSnap == VerticalSnap.BottomIn || m_verticalSnap == VerticalSnap.BottomOut) {
                anchorMin.y = m_target.anchorMin.y;    
                anchorMax.y = anchorMin.y;
                anchoredPosition.y = m_target.anchoredPosition.y - m_target.sizeDelta.y + (1.0f - m_target.pivot.y) * m_target.sizeDelta.y;
                
                if (m_verticalSnap == VerticalSnap.BottomOut) {
                    pivot.y = 1;
                    anchoredPosition.y -= m_deltaShift.y;
                } else {
                    pivot.y = 0;
                    anchoredPosition.y += m_deltaShift.y;
                }
            }
            
            // Top Snap.
            else if (m_verticalSnap == VerticalSnap.TopIn || m_verticalSnap == VerticalSnap.TopOut) {
                anchorMin.y = m_target.anchorMax.y;
                anchorMax.y = anchorMin.y;
                anchoredPosition.y = m_target.anchoredPosition.y + (1.0f - m_target.pivot.y) * m_target.sizeDelta.y;
                
                if (m_verticalSnap == VerticalSnap.TopOut) {
                    pivot.y = 0;
                    anchoredPosition.y += m_deltaShift.y;
                } else {
                    pivot.y = 1;
                    anchoredPosition.y -= m_deltaShift.y;
                }
            }
            
            // Rect transform update.
            rectTransform.anchorMin = anchorMin;
            rectTransform.anchorMax = anchorMax;
            rectTransform.anchoredPosition = anchoredPosition;
            rectTransform.pivot = pivot;
            
            // Updates tracker.
            m_Tracker.Add(this, rectTransform, 
                DrivenTransformProperties.PivotY |
                DrivenTransformProperties.AnchoredPositionY |
                DrivenTransformProperties.AnchorMinY |
                DrivenTransformProperties.AnchorMaxY);
        }
        
        //--------------------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------------------
        
        private void UpdateHorizontalSnap()  {                      
            // Current anchor info.
            Vector2 anchorMin = rectTransform.anchorMin;    
            Vector2 anchorMax = rectTransform.anchorMax;            
            Vector2 anchoredPosition = rectTransform.anchoredPosition;
            Vector2 pivot = rectTransform.pivot;
            
            // Center Snap.
            if (m_horizontalSnap == HorizontalSnap.Center) {
                anchorMin.x = (m_target.anchorMin.x + m_target.anchorMax.x) / 2.0f;    
                anchorMax.x = anchorMin.x;
                anchoredPosition.x = m_target.anchoredPosition.x + (0.5f - m_target.pivot.x) * m_target.sizeDelta.x;
                anchoredPosition.x += m_deltaShift.x;
                pivot.x = 0.5f;
            }
            
            // Right Snap.          
            else if (m_horizontalSnap == HorizontalSnap.RightIn || m_horizontalSnap == HorizontalSnap.RightOut)  {
                anchorMin.x = m_target.anchorMax.x;    
                anchorMax.x = anchorMin.x;
                anchoredPosition.x = m_target.anchoredPosition.x + m_target.sizeDelta.x - (m_target.pivot.x) * m_target.sizeDelta.x;
                
                if (m_horizontalSnap == HorizontalSnap.RightOut) {
                    pivot.x = 0;
                    anchoredPosition.x += m_deltaShift.x;
                } else {
                    pivot.x = 1;
                    anchoredPosition.x -= m_deltaShift.x;
                }
            }
            
            // Left Snap.
            else if (m_horizontalSnap == HorizontalSnap.LeftIn || m_horizontalSnap == HorizontalSnap.LeftOut) {
                anchorMin.x = m_target.anchorMin.x;    
                anchorMax.x = anchorMin.x;
                anchoredPosition.x = m_target.anchoredPosition.x - (m_target.pivot.x) * m_target.sizeDelta.x;
                
                if (m_horizontalSnap == HorizontalSnap.LeftOut) {
                    pivot.x = 1;
                    anchoredPosition.x -= m_deltaShift.x;
                } else {
                    pivot.x = 0;
                    anchoredPosition.x += m_deltaShift.x;
                }
            }
            
            // Rect transform update.
            rectTransform.anchorMin = anchorMin;
            rectTransform.anchorMax = anchorMax;
            rectTransform.anchoredPosition = anchoredPosition;
            rectTransform.pivot = pivot;      
                        
            // Updates tracker.
            m_Tracker.Add(this, rectTransform, 
                DrivenTransformProperties.PivotX |
                DrivenTransformProperties.AnchoredPositionX |
                DrivenTransformProperties.AnchorMinX |
                DrivenTransformProperties.AnchorMaxX);      
        }
        
        //--------------------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------------------
    }
}