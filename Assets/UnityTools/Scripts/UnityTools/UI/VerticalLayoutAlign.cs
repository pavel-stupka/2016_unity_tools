﻿/*
The MIT License (MIT)

Copyright (c) 2016 Pavel Stupka

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using UnityEngine.EventSystems;
using UnityEngine;
using UnityEngine.UI;

namespace UnityTools.UI {

    [AddComponentMenu("Layout/Vertical Layout Align", 201)]
    [ExecuteInEditMode]
    [RequireComponent(typeof(RectTransform))]
    public class VerticalLayoutAlign : UIBehaviour, ILayoutSelfController {
        
        // Direction and mode.
        public enum VerticalDirection { TopDown, BottomUp }
        public enum VerticalAlignMode { None, Simple, ResizeToFit, Cover, CoverWithBorders }
        
        // Direction.
        [SerializeField] private VerticalDirection m_verticalDirection = VerticalDirection.TopDown;
        public VerticalDirection verticalDirection { get { return m_verticalDirection; } set { m_verticalDirection = value; SetDirty(); }}
        
        // Mode.
        [SerializeField] private VerticalAlignMode m_verticalAlignMode = VerticalAlignMode.Simple;
        public VerticalAlignMode verticalAlignMode { get { return m_verticalAlignMode; } set { m_verticalAlignMode = value; SetDirty(); }}
        
        // Margin top.
        [SerializeField] private float m_marginTop = 0;
        public float marginTop { get { return m_marginTop; } set { m_marginTop = value; SetDirty(); }}        
        
        // Margin bottom.
        [SerializeField] private float m_marginBottom = 0;
        public float marginBottom { get { return m_marginBottom; } set { m_marginBottom = value; SetDirty(); }}

        // Rect transform.
        [System.NonSerialized]
        private RectTransform m_Rect;  
        
        // Driven Tracker.
        private DrivenRectTransformTracker m_Tracker;
        
        //--------------------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------------------
        //  PUBLIC
        //--------------------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------------------                     
        
        public virtual void SetLayoutHorizontal() {}
        
        //--------------------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------------------
        
        public virtual void SetLayoutVertical() {}
        
        //--------------------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------------------
        
        public void UpdateLayout() {
            SetDirty();
        }
        
        //--------------------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------------------
        //  PROTECTED
        //--------------------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------------------
        
        protected VerticalLayoutAlign() { }
        
        //--------------------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------------------
        
        protected override void OnEnable() {
            base.OnEnable();
            SetDirty();
        }
        
        //--------------------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------------------
        
        protected override void OnDisable() {
            m_Tracker.Clear();
            LayoutRebuilder.MarkLayoutForRebuild(rectTransform);
            base.OnDisable();
        }
        
        //--------------------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------------------
        
        protected override void OnRectTransformDimensionsChange() {
            AlignLayout();
        }
        
        //--------------------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------------------
                
        protected void SetDirty() {
            if (!IsActive()) {
                return;
            }                
            AlignLayout();
        }
        
        //--------------------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------------------
        
        #if UNITY_EDITOR
        
        protected override void OnValidate() {
            SetDirty();
        }

        #endif
        
        //--------------------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------------------
        //  PRIVATE
        //--------------------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------------------
        
        private RectTransform rectTransform  {
            get {
                if (m_Rect == null) {
                    m_Rect = GetComponent<RectTransform>();
                }                    
                return m_Rect;
            }
        }
        
        //--------------------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------------------
        
        private void AlignLayout()  {            
            m_Tracker.Clear();
            
            // No align.
            if (m_verticalAlignMode == VerticalAlignMode.None) {
                return;
            }
            
            // Preprocess child transforms.
            float childsHeight = 0;
            int activeChildCount = 0;
            for (int p = 0; p < rectTransform.childCount; p++) {                
                if (transform.GetChild(p).gameObject.activeSelf) {
                    childsHeight += PreprocessChild(transform.GetChild(p) as RectTransform);
                    activeChildCount++;
                }
            }            
            
            // Start position.
            float position = 0;
            float positionMul = 0;
            
            if (m_verticalDirection == VerticalDirection.TopDown) {
                position = -marginTop;
                positionMul = -1;
            } else {
                position = marginBottom;
                positionMul = 1;
            }
            
            // Spacing according to a layout.
            float spacing = 0;
            if (m_verticalAlignMode == VerticalAlignMode.Cover) {
                if (activeChildCount > 1) {
                    spacing = (rectTransform.rect.height - marginTop - marginBottom - childsHeight) / (float)(activeChildCount - 1);
                } else if (activeChildCount == 1) {
                    float startDelta = spacing = (rectTransform.rect.height - childsHeight) / (float)(activeChildCount + 1);
                    position += positionMul * startDelta;
                }
            } else if (m_verticalAlignMode == VerticalAlignMode.CoverWithBorders) {
                spacing = (rectTransform.rect.height - marginTop - marginBottom - childsHeight) / (float)(activeChildCount + 1);
                position += positionMul * spacing;
            }
            
            // Iterate child transforms.
            for (int p = 0; p < rectTransform.childCount; p++) {
                if (transform.GetChild(p).gameObject.activeSelf) {
                    position += (positionMul * ProcessChild(transform.GetChild(p) as RectTransform, position));
                    position += positionMul * spacing;
                }
            }
                        
            // Total size.
            float totalSize = positionMul * position + marginBottom;
            
            // Resize mode?
            if (m_verticalAlignMode == VerticalAlignMode.ResizeToFit) {                
                m_Tracker.Add(this, rectTransform, DrivenTransformProperties.SizeDeltaY);                
                rectTransform.sizeDelta = new Vector2(rectTransform.sizeDelta.x, totalSize);
            }
        }
        
        //--------------------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------------------
        
        // Returns delta size to be added to a position.
        private float ProcessChild(RectTransform childTransform, float position) {
            
            // Acnhor and position.
            Vector2 anchoredPosition = childTransform.anchoredPosition;
                        
            // Padding.
            float padding = 0;
            float totalPadding = 0;
            LayoutElementPadding layoutElementPadding = childTransform.gameObject.GetComponent<LayoutElementPadding>();
            if (layoutElementPadding != null) {                
                if (m_verticalDirection == VerticalDirection.TopDown) {
                    padding = layoutElementPadding.paddingTop;
                } else {
                    padding = layoutElementPadding.paddingBottom;
                }
                totalPadding = layoutElementPadding.paddingTop + layoutElementPadding.paddingBottom;                
            }            
            
            // Sets the new position.
            if (m_verticalDirection == VerticalDirection.TopDown) {
                anchoredPosition.y = position - padding;
            } else {
                anchoredPosition.y = position + padding;
            }            
            
            // Sets new Values;
            childTransform.anchoredPosition = anchoredPosition;
                        
            return childTransform.sizeDelta.y + totalPadding;
        }
        
        //--------------------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------------------
        
        // Child preprocessing. Returns child size including padding if any.
        private float PreprocessChild(RectTransform childTransform) {
            
            // Driven stuff.
            m_Tracker.Add(this, childTransform, 
                DrivenTransformProperties.PivotY |
                DrivenTransformProperties.AnchoredPositionY |
                DrivenTransformProperties.AnchorMinY |
                DrivenTransformProperties.AnchorMaxY);
            
            // Anchors.
            Vector2 anchorMin = childTransform.anchorMin;
            Vector2 anchorMax = childTransform.anchorMax;            
            Vector2 pivot = childTransform.pivot;
            
            // Anchor values.
            if (m_verticalDirection == VerticalDirection.TopDown) {
                anchorMax.y = 1;
                anchorMin.y = 1;
                pivot.y = 1;
            } else {
                anchorMax.y = 0;
                anchorMin.y = 0;
                pivot.y = 0;
            }            
            
            // Updates anchors.
            childTransform.anchorMin = anchorMin;
            childTransform.anchorMax = anchorMax;
            childTransform.pivot = pivot;
            
            // Recursive child update.
            UpdateChild(childTransform);            
            
            // Do we have a margin?
            float totalPadding = 0;
            LayoutElementPadding layoutElementPadding = childTransform.gameObject.GetComponent<LayoutElementPadding>();
            if (layoutElementPadding != null) {
                totalPadding = layoutElementPadding.paddingTop + layoutElementPadding.paddingBottom;                
            }   
            
            return childTransform.sizeDelta.y + totalPadding;
        }
        
        //--------------------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------------------
        
        // Updates child's structure.
        private void UpdateChild(RectTransform childTransform) {
            
            // Forces nested layout to be updated.
            VerticalLayoutAlign verticalLayoutAlign = childTransform.gameObject.GetComponent<VerticalLayoutAlign>();
            if (verticalLayoutAlign != null) {
                verticalLayoutAlign.UpdateLayout();
            }
            
            // Forces content size fitter to be updated.
            ContentSizeFitter contentSizeFitter = childTransform.gameObject.GetComponent<ContentSizeFitter>();
            if (contentSizeFitter != null) {
                contentSizeFitter.SetLayoutHorizontal();
                contentSizeFitter.SetLayoutVertical();
            }
                        
        }
        
        //--------------------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------------------
        
        // Check for updates
        private void Update()  {   
            
            // Observe childs for changes.         
            bool childsChanged = false;
            
            for (int p = 0; p < rectTransform.childCount; p++) {
                if ((transform.GetChild(p) as RectTransform).hasChanged) {
                    childsChanged = true;
                    break;
                }
            }
            
            if (childsChanged) {
                SetDirty();
            }
        }
        
        //--------------------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------------------
    }
}