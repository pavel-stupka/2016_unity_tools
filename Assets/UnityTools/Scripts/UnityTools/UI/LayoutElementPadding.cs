﻿/*
The MIT License (MIT)

Copyright (c) 2016 Pavel Stupka

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

using UnityEngine.EventSystems;
using UnityEngine;
using UnityEngine.UI;

namespace UnityTools.UI {

    [AddComponentMenu("Layout/Layout Element Padding", 202)]
    [ExecuteInEditMode]
    [RequireComponent(typeof(RectTransform))]
    public class LayoutElementPadding : UIBehaviour {
        
        // padding top.
        [SerializeField] private float m_paddingTop = 0;
        public float paddingTop { get { return m_paddingTop; } set { m_paddingTop = value; SetDirty(); }}
        
        // padding bottom.
        [SerializeField] private float m_paddingBottom = 0;
        public float paddingBottom { get { return m_paddingBottom; } set { m_paddingBottom = value; SetDirty(); }}
        
        // padding left.
        [SerializeField] private float m_paddingLeft = 0;
        public float paddingLeft { get { return m_paddingLeft; } set { m_paddingLeft = value; SetDirty(); }}
        
        // padding right.
        [SerializeField] private float m_paddingRight = 0;
        public float paddingRight { get { return m_paddingRight; } set { m_paddingRight = value; SetDirty(); }}
        
        //--------------------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------------------
        //  PROTECTED
        //--------------------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------------------
        
        protected LayoutElementPadding() { 
            
        }
        
        //--------------------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------------------
        
        protected override void OnEnable() {
            base.OnEnable();
            SetDirty();
        }
        
        //--------------------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------------------

        protected override void OnTransformParentChanged() {
            SetDirty();
        }
        
        //--------------------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------------------

        protected override void OnDisable() {
            SetDirty();
            base.OnDisable();
        }
        
        //--------------------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------------------

        protected override void OnDidApplyAnimationProperties() {
            SetDirty();
        }
        
        //--------------------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------------------

        protected override void OnBeforeTransformParentChanged() {
            SetDirty();
        }
        
        //--------------------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------------------

        protected void SetDirty() {
            if (!IsActive()) {
                return;
            }                
            LayoutRebuilder.MarkLayoutForRebuild(transform as RectTransform);
        }
        
        //--------------------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------------------

        #if UNITY_EDITOR
    
        protected override void OnValidate() {
            SetDirty();
        }

        #endif

        //--------------------------------------------------------------------------------------------------------------
        //--------------------------------------------------------------------------------------------------------------
    }
}